# Sequence Query web application

## Contents
+ [About](#markdown-header-about)
+ [Installation](#markdown-header-installation)
    - [JAR](#markdown-header-jar)
    - [WAR](#markdown-header-war)
    - [Build from sources](#markdown-header-build-from-sources)
+ [Usage](#markdown-header-usage)


# About
This is the repository for the web version of Sequence Query. It is a Java(7) web application using Springs web framework
for the back-end and AngularJS for the frontend. The repository for the command-line version can be found
[here](https://bitbucket.org/harmbrugge/sequencequery).

Sequence Query is a fast way to search a FASTA file format, by filtering it's content by multiple search options:
### Search options ###
  * PROSITE pattern: Will report all sequences that contain the Prosite pattern. the pattern will be high-lighted
  in a modal if requested
  * Regular expression pattern: Will report all sequences that contain the Regular expression pattern. the pattern will
  be high-lighted in a modal if requested
  * Organism name:  Will report all sequences having this wildcard string (a regex pattern) in the organism name.
  * Description: Will report all sequences having this wildcard string (a regex pattern) in the description / sequence
  name
  * Identifier: Will report all sequences having this wildcard string (a regex pattern) in the identifiers of the
  sequence
  
  
# Installation

Important: For data storage in the pre-build WAR and JAR files, an in-memory H2 database is used. Resulting in data loss
between application launches. If you want your data to persist between application launches, make sure to
configure your own datasource, explained [here](#markdown-header-configuration)

### JAR

Because this is a Spring boot project, it can be run using the embedded Tomcat server supplied by spring boot.
The pre-build jar file can be found in the [download section](https://bitbucket.org/harmbrugge/sequencequeryweb/downloads).

If Java 7 or greater is installed the application can be started as follow:
```bash
java -jar sequence-query.jar
```

The embedded tomcat server will be initialized on port 8080, so make sure you have no processes running on this port.
After the application has started, it will be available at: http://localhost:8080

### WAR

If you want to deploy this application on a running Tomcat server you can use the pre-build WAR file in the
[download section](https://bitbucket.org/harmbrugge/sequencequeryweb/downloads).
Keep in mind that an in-memory database is used, and you should configure your own datasource for data persistence.


### Build from sources

Firstly check-out this repository using your favourite git client or IDE.
To manage external dependencies gradle is used. The gradle build file is located in the root and a default gradle
wrapper is present in the /gradle directory. If you're not familiar with gradle, a
[tutorial](https://spring.io/guides/gs/gradle/) can be found here on how to build the project using the command-line.
Although most IDE's have build-in gradle support, or plug-ins will be available for gradle build support.

To use the default gradle wrapper manually use to following command when located in the projects directory:
```bash
./gradlew build
```

After building the project, the main class (com.harmbrugge.sequence_query.web.SequenceQueryApplication.java) can be
run to start the web-server.
Or use use the gradle wrapper again:
```bash
./gradlew bootRun
```

# Configuration

The configuration file is located in src/main/application.properties. The default in-memory H2 database is configured
here and additional configurations can be managed in this file.
For common spring boot configurations see
[Spring documentation](https://docs.spring.io/spring-boot/docs/current/reference/html/common-application-properties.html)

### Datasource

Example how to configure a MySQL database (connection dependencies are included in the gradle build script)
Remove the H2 database configuration:
```
spring.datasource.url=jdbc:h2:mem:testdb;Mode=Oracle
```
Add the MySQL server destination and user credentials:
``` 
spring.datasource.url=jdbc:mysql://<host>:3306/<database>
spring.datasource.username=<user>
spring.datasource.password=<password>
```
If you wish to use a a different database server you have to include the corresponding dependencies in de build.gradle
file. See [this](https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-sql.html) documentation
for more information how to achieve this.

# Usage

### Create user
After starting the application a user can be created in the login menu:

![Log in](https://bitbucket.org/harmbrugge/sequencequeryweb/raw/master/meta/login.png)

Or a dummy user is provided in the JAR and WAR builds in the download section:

 + Username : dummy
 + Password : 12345678
 
### Search

When logged in a search can be performed by clicking the new search button in the menu.
The search form will be presented and one or more options can be used:

![New search](https://bitbucket.org/harmbrugge/sequencequeryweb/raw/master/meta/new-search.png)


The sequences complying to the search options will be shown underneath the search form:

![Result](https://bitbucket.org/harmbrugge/sequencequeryweb/raw/master/meta/result.png)


If the actual biological sequence is searched, the hit on the sequence can be shown by clicking the sequence:

![Sequence](https://bitbucket.org/harmbrugge/sequencequeryweb/raw/master/meta/sequence.png)

Enjoy!