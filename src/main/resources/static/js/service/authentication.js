/**
 * Service for authorization request
 * Holds the current authorization information
 */
sequenceQuery.factory('$auth', function ($http) {
    // Initial credentials
    var authentication = {
        user: {
            authenticated: false
        }
    };

    /**
     * Authenticates the user by sending an authorization header
     * and user credentials
     *
     * @param user posted user object
     * @returns {HttpPromise} the data promise
     */
    authentication.authenticate = function (user) {
        var headers = user ? {
            authorization: "Basic "
            + btoa(user.username + ":" + user.password)
        } : {};

        return $http.get('user', {headers: headers})
    };

    /**
     * Checks if there is an active user on the session
     * Will return 204 status (No content) if there is no active user on the session
     *
     * @returns {HttpPromise} the data promise
     */
    authentication.checkAuthentication = function () {
        return $http.get('user');
    };

    /**
     * Logs out the user.
     *
     * @returns {HttpPromise} the data promise
     */
    authentication.logout = function () {
        return $http.post('logout')
    };

    /**
     * Reset authorization credentials
     */
    authentication.reset = function () {
        authentication.user = {
            authenticated: false
        }
    };

    /**
     * Handles the new account generation post.
     * User will be validated in back-end, resulting in 405(Bad request)
     * if credentials are incorrect or a 409(Conflict) if the username or
     * mail already exists.
     *
     * @param user the user object
     * @returns {HttpPromise} the data promise
     */
    authentication.newUser = function (user) {
        return $http.post('user', user)
    };

    return authentication;
});
