/**
 * Handles the alerts.
 */
sequenceQuery.factory('$alert', function ($mdToast, $mdDialog) {
    var alert = {};

    /**
     * Shows an info toast (info bar) for 3 seconds
     * @param content the content in the toast
     */
    alert.showInfo = function (content) {
        $mdToast.show(
            $mdToast.simple()
                .textContent(content)
                .hideDelay(3000)
                .position('top left')
        );
    };

    /**
     * Show an error modal
     * @param content the content in the modal
     */
    alert.showErrorModal = function (content) {
        $mdDialog.show(
            $mdDialog.alert()
                .parent(angular.element(document.body))
                .clickOutsideToClose(true)
                .title('Error')
                .textContent(content)
                .ok('Ok')
        );
    };

    return alert;
});
