/**
 * Handles and saves the active site content.
 */
sequenceQuery.factory('$siteContent', function ($rootScope) {

    // Initial content
    var siteContent = {
        menuItems: {
            '/': {name: 'Home', url: '#/'}
        },
        title: 'Home'
    };

    /**
     * Changes the title and informs listeners
     * @param title the title to be set
     */
    siteContent.setTitle = function (title) {
        siteContent.title = title;
        $rootScope.$broadcast('$siteContent:titleChanged')
    };

    /**
     * Determines the content by authority
     * @param user user object
     */
    siteContent.determineContent = function (user) {

        // fall-through (awesome)
        switch (user.authority) {
            case 'ADMIN':
                siteContent.menuItems['/admin'] = {
                    name: 'Users',
                    url: '#/users'
                };
            case 'USER':
                siteContent.menuItems['/search'] = {
                    name: 'New search',
                    url: '#/search'
                };
                siteContent.menuItems['/history'] = {
                    name: 'History',
                    url: '#/history'
                };
                break;
            default:
                siteContent.menuItems = {
                    '/': {name: 'Home', url: '#/'}
                }
        }
    };

    return siteContent;
});