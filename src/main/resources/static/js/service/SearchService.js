/**
 * Service layer for sequence query search requests
 */
sequenceQuery.factory('$searchService', ['$http', function($http) {
    return {

        /**
         * Gets the sequences for a search request
         *
         * @param id the search requests id
         * @returns {HttpPromise} the data promise
         */
        getSequences: function(id) {
            return $http.get('sequences/' + id)
        },

        /**
         * Gets all the search requests
         *
         * @returns {HttpPromise} the data promise
         */
        getSearchRequests: function() {
            return $http.get('searches/')
        },

        /**
         * Gets a search request
         *
         * @returns {HttpPromise} the data promise
         */
        getSearchRequest: function(id) {
            return $http.get('search-request/' + id)
        },

        /**
         * Posts a search request.
         *
         * @param searchRequest the search request
         * @param file FASTA file
         * @returns {HttpPromise} the data promise
         */
        doSearchRequest: function(searchRequest, file) {
            var formData = new FormData();

            // todo: clean the input parameters in a better way
            if (file) {
                formData.append('file', file);
            }
            if (searchRequest.identifier) {
                formData.append('identifier', searchRequest.identifier);
            }
            if (searchRequest.regex) {
                formData.append('regex', searchRequest.regex);
            }
            if (searchRequest.prosite) {
                formData.append('prosite', searchRequest.prosite);
            }
            if (searchRequest.organism) {
                formData.append('organism', searchRequest.organism);
            }
            if (searchRequest.description) {
                formData.append('description', searchRequest.description);
            }

            return $http.post('search-request/', formData, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined}
                })
        }

    };

}]);