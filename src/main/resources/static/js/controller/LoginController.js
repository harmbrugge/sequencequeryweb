/**
 * Controller for login & logout
 */
sequenceQuery.controller('LoginController',
    function ($rootScope, $scope, $alert, $siteContent, $auth, $location) {

        $siteContent.setTitle('Login');

        /**
         * Login, informs navigation controller when login succeeds
         * to refresh page content
         */
        $scope.login = function () {
            $auth.authenticate($scope.user)
                .success(function (user) {
                    $alert.showInfo("Hello " + user.username);
                    // Need a random get to sync csrf cookie?
                    $auth.checkAuthentication();
                    $auth.user = user;
                    $auth.user.authenticated = true;
                    $rootScope.$broadcast('$auth:changed');
                    $location.path("/");
                })
                .error(function () {
                    $alert.showInfo("Login failed, try again")
                });
        };

        /**
         * Logout, informs navigation controller when logged out
         * to refresh page content
         */
        $scope.logout = function () {
            $auth.logout()
                .success(function () {
                    $alert.showInfo('Bye ' + $auth.user.username);
                    $auth.reset();
                    $rootScope.$broadcast('$auth:changed');
                    $location.path("/");
                })
                .error(function () {
                    console.error("Logout failed");
                });
        };

    });
