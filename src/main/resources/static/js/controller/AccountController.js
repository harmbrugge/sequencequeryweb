/**
 * Controller for account creation
 */
sequenceQuery.controller('AccountController',
    function ($auth, $scope, $alert, $location, $siteContent) {

        // Set the title
        $siteContent.setTitle('Create new account');

        // New user object for form
        $scope.user = {};

        /**
         * Submit a user and redirect to login when success.
         */
        $scope.submitUser = function () {
            $auth.newUser($scope.user)
                .success(function () {
                    $alert.showInfo('Account created');
                    $location.path('/login');
                })
                .error(function (error) {
                    if (error.message) {
                        $alert.showInfo(error.message)
                    }
                })
        };

    });
