/**
 * Navigation controller (root controller)
 * Handles te site content and listens to changes
 */
sequenceQuery.controller('NavigationController',
    function ($scope, $location, $alert, $siteContent, $auth) {

        /**
         * Check to see if there is an active session with a logged in user
         * on page load
         */
        $auth.checkAuthentication()
            .success(function (user) {
                if (user) {
                    $auth.user = user;
                    $auth.user.authenticated = true;
                    $scope.user = $auth.user;
                }
                $siteContent.determineContent($auth.user);
                $scope.menuItems = $siteContent.menuItems;
            });

        /**
         * Get the basic menu items on initial load.
         */
        $scope.menuItems = $siteContent.menuItems;

        /**
         * Listens to changes in authorization and loads new content
         */
        $scope.$on('$auth:changed', function () {
            $siteContent.determineContent($auth.user);
            $scope.menuItems = $siteContent.menuItems;
            $scope.user = $auth.user;
        });

        /**
         * Listens to changes in page content.
         */
        $scope.$on('$siteContent:titleChanged', function () {
            $scope.title = $siteContent.title;
        });

    });