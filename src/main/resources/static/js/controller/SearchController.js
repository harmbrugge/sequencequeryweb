/**
 * Controller for handling search requests and sequence presentation.
 */
sequenceQuery.controller('SearchController',
    function ($scope, $routeParams, $searchService, $alert, $siteContent, $mdDialog) {


        $siteContent.setTitle('New search');

        /**
         * Getter for sequences belonging to a search request
         *
         * @param id the search request id
         */
        $scope.getSequences = function (id) {
            $searchService.getSequences(id)
                .success(function (sequences) {
                    $scope.sequences = sequences;
                })
        };

        /**
         * Getter for all the search requests
         */
        $scope.getSearchRequests = function () {
            $searchService.getSearchRequests()
                .success(function (searchRequests) {
                    $scope.searchRequests = searchRequests;
                })
        };

        /**
         * Getter for a search requests
         */
        $scope.getSearchRequest = function (id) {
            $searchService.getSearchRequest(id)
                .success(function (searchRequest) {
                    $scope.searchRequest = searchRequest;
                    $scope.getSequences(id);
                })
        };

        /**
         * Handles form submission for a search request.
         */
        $scope.submit = function () {
            $scope.loading = true;
            $searchService.doSearchRequest($scope.searchRequest, $scope.fastaFile)
                .success(function (searchId) {
                    $scope.getSequences(searchId);
                    $scope.getSearchRequests();
                    $scope.loading = false;
                })
                .error(function (exception) {
                    $alert.showErrorModal(exception.message);
                    $scope.loading = false;
                });
        };

        /**
         * Shows a dialog for a sequence object.
         *
         * @param sequence the sequence to show
         */
        $scope.showSequence = function (sequence) {
            $mdDialog.show({
                locals: {sequence: sequence},
                controller: ['$scope', 'sequence', function ($scope, sequence) {
                    $scope.hide = function () {
                        $mdDialog.hide();
                    };
                    $scope.sequence = sequence;
                }],
                templateUrl: 'templates/sequence.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true
            });
        };

        /**
         * Holds the selected sequences in the view
         *
         * @type {Array}
         */
        $scope.selected = [];

        /**
         * Holds table search options
         *
         * @type {{filter: string, order: string, limit: number, page: number}}
         */
        $scope.query = { filter: '', order: 'externalId', limit: 10, page: 1 };

        // Cal getter for initial view
        $scope.getSearchRequests();

        // Load the corresponding search request if a path variable is present
        if ($routeParams.id) {
            $scope.getSearchRequest($routeParams.id)
        }
    });
