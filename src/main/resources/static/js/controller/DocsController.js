/**
 * Controller for document page
 */
sequenceQuery.controller('DocsController',
    function ($siteContent) {

        // Set the title
        $siteContent.setTitle('Sequence Query / Documentation');

    });
