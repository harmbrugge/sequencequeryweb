/**
 * Filter to highlighting a search result on a sequence
 */
sequenceQuery.filter('highlight', function ($sce) {
    return function (sequence, start, end) {
        // Start & stop are positions, not indexes
        start--;
        end--;

        sequence = sequence.substr(0, start) +
            '<span class="highlight">' +
            sequence.substr(start, end - start) +
            '</span>' +
            sequence.substr(end);

        return $sce.trustAsHtml(sequence)
    }
});
