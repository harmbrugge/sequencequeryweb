/**
 * App configuration.
 */
var sequenceQuery = angular.module('sequenceQuery', ['ngRoute', 'ngMaterial', 'md.data.table'])
    .config(function ($mdThemingProvider, $routeProvider, $httpProvider) {

        /**
         * Theming for angular material
         */
        $mdThemingProvider.theme('default')
            .primaryPalette('blue-grey')
            .accentPalette('orange');

        /**
         * Routing provider
         */
        $routeProvider
            .when('/', {
                templateUrl: 'templates/docs.html',
                controller: 'DocsController'
            })
            .when('/search/:id?', {
                templateUrl: 'templates/search.html',
                controller: 'SearchController'
            })
            .when('/login', {
                templateUrl: 'templates/login.html',
                controller: 'LoginController'
            })
            .when('/new-account', {
                templateUrl: 'templates/new-account.html',
                controller: 'AccountController'
            })
            .when('/history', {
                templateUrl: 'templates/history.html',
                controller: 'SearchController'
            })
            .otherwise({
                redirectTo: '/'
            });

        /**
         * XML headers needed for authorization
         *
         * @type {string}
         */
        $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';

    })
    ;

/**
 * Custom directive for file handling in form.
 */
sequenceQuery.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function () {
                scope.$apply(function () {
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);
