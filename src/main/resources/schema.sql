DROP TABLE IF EXISTS search_results;
DROP TABLE IF EXISTS sequences;
DROP TABLE IF EXISTS search_requests;
DROP TABLE IF EXISTS users;


CREATE TABLE IF NOT EXISTS users (
  id             INT AUTO_INCREMENT,
  username       VARCHAR(40) UNIQUE,
  authority      VARCHAR(45),
  name           VARCHAR(45),
  surname        VARCHAR(45),
  email          VARCHAR(45) UNIQUE,
  password       VARCHAR(80),
  enabled        TINYINT,
  password_token VARCHAR(200),
  timestamp      DATETIME,
  PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS search_requests (
  id          INT(11) NOT NULL AUTO_INCREMENT,
  user_id     INT(11) NOT NULL,
  date        DATETIME,
  file_name   VARCHAR(45),
  identifier  VARCHAR(45),
  description VARCHAR(45),
  organism    VARCHAR(45),
  regex       VARCHAR(45),
  prosite     VARCHAR(45),
  PRIMARY KEY (id),
  FOREIGN KEY (user_id) REFERENCES users (id)
  ON DELETE CASCADE
  ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS sequences (
  id                INT(11) NOT NULL AUTO_INCREMENT,
  search_request_id INT(11) NOT NULL,
  external_id       VARCHAR(45)      DEFAULT NULL,
  description       TEXT,
  organism          VARCHAR(100)     DEFAULT NULL,
  sequence          TEXT,
  fasta_header      TEXT,
  sequence_type     VARCHAR(10)      DEFAULT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (search_request_id) REFERENCES search_requests (id)
  ON DELETE CASCADE
  ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS search_results (
  id            INT(11) NOT NULL AUTO_INCREMENT,
  sequence_id   INT(11) NOT NULL,
  start         INT(11) NOT NULL,
  stop          INT(11) NOT NULL,
  search_option VARCHAR(10)      DEFAULT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (sequence_id) REFERENCES sequences (id)
  ON DELETE CASCADE
  ON UPDATE CASCADE
);