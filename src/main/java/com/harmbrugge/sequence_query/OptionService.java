/*
 * Copyright (c) 2015 Harm Brugge [harmbrugge@gmail.com].
 * All rights reserved.
 */
package com.harmbrugge.sequence_query;

/**
 * Interface for options parsing for SequenceQuery. Supported options are listed
 * in {@link SearchOption}. Besides these search options, the path to the FASTA
 * file and a {@link FastaListener} should be be created, so that a
 * {@link FastaHandler} object can be initialized with the desired actions.
 *
 * A {@link SequenceSearcher} object could be used in the implementation of a
 * {@link FastaListener} for a default implementations see
 * {@link com.harmbrugge.sequence_query.listener.SeqSearchListener}
 *
 * @see SequenceQuery
 * @see FastaHandler
 * @see FastaListener
 * @see SearchOption
 *
 * @author Harm Brugge
 */
public interface OptionService {
    /**
     * Returns the path to the FASTA file.
     *
     * @return path to FASTA file
     */
    String getInputPath();

    /**
     * Returns a FastaListener implementation. used for listening to a
     * {@link FastaHandler} object.
     *
     * @return the FastaListener implementation.
     */
    FastaListener getListener();

    /**
     * Returns true if the options contain a request for a summary.
     *
     * @return true if contains summary
     */
    boolean hasSummary();
}
