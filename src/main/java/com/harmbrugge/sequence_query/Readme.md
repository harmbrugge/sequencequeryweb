# SequenceQuery #

## An application for parsing and searching a FASTA file ##
This application can be used to query a FASTA file. Multiple filter option are available and listed below.

## Usage ##
Binaries are available in the [download section](https://bitbucket.org/harmbrugge/sequencequery/downloads)
and can be used as follow:
```java -jar SequenceQuery.jar --input path/to/fasta.fa [search options]```

### Search options ###
  * --fetch_prosite <PROSITE PATTERN>  Will report all sequences that contain the Prosite pattern, 
    with the location and actual sequence found (see below for example output).
  * --find_regex <REGEX_PATTERN>   Will report all sequences that contain the Regular expression pattern, 
    with the location and actual sequence found (see below for example output).
  * --find_organism <(PART OF) ORGANISM_NAME>  Will report all sequences having this wildcard string (a regex pattern)
    in the organism name
  * --find_description <WILDCARD-STRING>  Will report all sequences having this wildcard string (a regex pattern)
    in the description / sequence name

The filter options can be used in any combination. If more than one option is used, only sequences that comply to all the option will be shown.

### Use cases ###

  1. ```java -jar SequenceQuery.jar --input path/to/fasta.fa --to_csv ";"```   
    Accepts a command-line argument specifying the (multi-)sequence file.
    Generates a nicely formatted csv listing with these columns ands the given character as separator:  
    * ACCNO: First accession
    * NAME: Name / descripion
    * ORGANISM: Organism
    * TYPE: Type (DNA, RNA, Protein)
    * LENGHT: Length
    * MOL_WEIGHT: Molecular weight
  2. ```java -jar SequenceQuery.jar --input path/to/fasta.fa --summary```   
    Creates a textual summary of the parsed file: number of sequences, sequence type and average length
  3. ```java -jar SequenceQuery.jar --input path/to/fasta.fa --find_regex HV+Q --find_organism sapiens```   
    Will output all the sequences that contain the regular expression pattern HV+Q and where the organism's name contains sapiens.

### Output format ###
  1. Searches containing sequence searches(--fetch_prosite, --find_regex) will generate the following CSV output separated by ";":  
    * ACCNO: First accession
    * NAME: Name / description
    * ORGANISM: Organism
    * TYPE: Type (DNA, RNA, Protein)
    * POSITION: Start position of match
    * SEQ: Actual sequence of match
  2. Searches without sequence search options will generate FASTA output
  
## Source ##
For library usage, Javadoc is available [here](http://harmbrugge.bitbucket.org/sequence-query/index.html?com/harmbrugge/sequence_query/package-summary.html)