/*
 * Copyright (c) 2015 Harm Brugge [harmbrugge@gmail.com].
 * All rights reserved.
 */
package com.harmbrugge.sequence_query;

import java.util.ArrayList;
import java.util.List;

/**
 * Class responsible for searching a sequence object. {@link Sequence}.
 * Search filters should be passed in at construction: {@link Filter},
 * {@link SequenceSearcher#SequenceSearcher(List)}.
 * A sequence object can be searched by using the search method
 * {@link SequenceSearcher#search(Sequence)}
 *
 * @author Harm Brugge
 */
public class SequenceSearcher {

    /**
     * List of search filters. initialized at object construction
     */
    private final List<Filter> searchFilters;

    /**
     * Search results. reset after every search
     */
    private List<SearchResult> results;

    /**
     * Count of sequences that comply to the search filters.
     */
    private int foundCount;

    /**
     * Keeps track of total searched sequences.
     */
    private int totalCount;

    /**
     * Reference to sequence that is processed. reset after every search
     */
    private Sequence sequence;


    /**
     * Constructor with SearchOptions.
     *
     * @param filters List of search filters
     */
    public SequenceSearcher(final List<Filter> filters) {
        this.results = new ArrayList<>();
        this.searchFilters = filters;
    }

    /**
     * Searches a sequence with the search options provided at object creation.
     * Stores search results for every sequence searches
     *
     * @param seq Sequence to be searched
     * @return true when complies to filters
     */
    public final boolean search(final Sequence seq) {
        this.results = new ArrayList<>();
        this.sequence = seq;

        if (searchFilters.isEmpty()) {
            return false;
        }

        for (Filter filter :searchFilters) {
            if (filter.search(seq)) {
                // not all the filters store search results
                if (filter.getSearchResult() != null) {
                    results.add(filter.getSearchResult());
                }
            } else {
                totalCount++;
                return false;
            }
        }
        foundCount++;
        totalCount++;
        return true;
    }

    /**
     * Gets the search results.
     *
     * @return list of search results
     */
    public final List<SearchResult> getResults() {
        return results;
    }

    /**
     * Converts the sequence with search results to CSV string.
     *
     * @return CSV formatted string
     */
    public final String resultsToCsv() {
        StringBuilder sb = new StringBuilder();
        char sep = ';';
        for (SearchResult result : results) {
            sb.append(sequence.getId()).append(sep)
                    .append(sequence.getDescription()).append(sep)
                    .append(sequence.getOrganism()).append(sep)
                    .append(sequence.getSequenceType()).append(sep)
                    .append(result.getStart()).append(sep)
                    .append(result.getResult()).append(System.lineSeparator());
        }
        return sb.toString();
    }

    /**
     * Gets the count of sequences that complied to the filters.
     *
     * @return sequence count
     */
    public final int getFoundCount() {
        return foundCount;
    }

    /**
     * Gets the count of processed sequences.
     *
     * @return sequence count
     */
    public final int getCount() {
        return totalCount;
    }

    /**
     * Checks if sequence search results are found.
     *
     * @return true/false containing search results
     */
    public final boolean hasSeqResult() {
        return !results.isEmpty();
    }

    /**
     * Checks if any filters are initialized.
     *
     * @return true/false containing filters
     */
    public final boolean hasFilter() {
        return !searchFilters.isEmpty();
    }

    @Override
    public String toString() {
        return "SequenceSearcher{" + "searchFilters=" + searchFilters + ", foundCount=" + foundCount
                + ", totalCount=" + totalCount + '}';
    }


}
