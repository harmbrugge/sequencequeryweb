/*
 * Copyright (c) 2015 Harm Brugge [harmbrugge@gmail.com].
 * All rights reserved.
 */
package com.harmbrugge.sequence_query;

import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class parses a FASTA file into sequence objects. Streaming is supported when using a
 * FastaListener {@link FastaListener} while constructing {@link #FastaHandler(MultipartFile, FastaListener)}.
 * If the listener is absent, sequence will be added to a list and can be obtained via {@link #getSequences()}
 *
 * @see Sequence
 * @see FastaListener
 *
 * @author Harm Brugge
 * @version 0.0.1
 */
public class FastaHandler {

    /**
     * Regular expression used to find the organism name in description.
     */
    private static final String ORGANISM_REG_EX = "^\\s*(.+)\\s+\\[(.+)]$";

    /**
     * Length to distinguish id from description in FASTA header. if the length
     * of an identifier is smaller than, or equal to 3 it's probably a
     * description of the id and the actual id is the next term: >gb|FS43253135
     * ,gi|32532454 or lcl|3151
     */
    private static final int MIN_ID_LENGTH = 3;

    /**
     * Path to the FASTA file.
     */
    private final MultipartFile file;

    /**
     * List of sequences. only used when without listener
     */
    private final List<Sequence> sequences;

    /**
     * Optional listener. sequences won't be stored if present
     */
    private final FastaListener fastaListener;

    /**
     * Sequence type in the FASTA file.
     */
    private SequenceType sequenceType;

    /**
     * Keeps track of the amount of processed sequences.
     */
    private int sequenceCount;

    /**
     * Total length of all the processed sequences.
     */
    private int totalLength;

    /**
     * Keeps track of the number of invalid sequences.
     */
    private int invalidCount;

    /**
     * Matcher is reused for every sequence.
     */
    private Matcher organismMatcher;


    /**
     * Constructor with listener. sequences won't be stored
     *
     * @param file FASTA file
     * @param sequenceListener will be notified when sequence is created or file
     * is read
     */
    public FastaHandler(final MultipartFile file,
            final FastaListener sequenceListener) {
        this.file = file;
        this.sequences = new ArrayList<>();
        this.fastaListener = sequenceListener;
        this.organismMatcher = Pattern.compile(ORGANISM_REG_EX).matcher("");
    }

    /**
     * Constructor without listener. sequences will be stored
     *
     * @param file path to the FASTA file
     */
    public FastaHandler(final MultipartFile file) {
        this(file, null);
    }

    /**
     * Reads the FASTA file and creates sequence objects. If constructed with
     * listener, listener will be notified, else sequences will be stored.
     *
     * @throws IOException if file is not readable
     */
    public final void readFile() throws IOException {

        try (BufferedReader br = new BufferedReader(new InputStreamReader(file.getInputStream()))) {
            String line = br.readLine();
            if (!validFastaHeader(line)) {
                throw new IllegalArgumentException("Illegal FASTA header");
            }

            notifyStart();
            StringBuilder sb = new StringBuilder();
            Sequence sequence = null;

            while (line != null) {
                // Search for fasta header
                if (validFastaHeader(line)) {
                    // Only not true at first entry
                    if (sequence != null) {
                        // Adds string builder to sequence object
                        addSequence(sequence, sb);
                        notifyOrStore(sequence);
                        sb = new StringBuilder();
                    }
                    // Returns new sequence object
                    sequence = parseHeader(line);
                } else {
                    sb.append(line.toUpperCase());
                }
                line = br.readLine();
            }
            // process last sequence
            addSequence(sequence, sb);
            notifyOrStore(sequence);
            notifyFinished();
        }
    }

    /**
     * Creates a sequence object out of a FASTA header.
     *
     * @param header FASTA header, must obey to FASTA conventions
     * @return Sequence object
     * @throws IllegalArgumentException if header is in bad format
     */
    private Sequence parseHeader(final String header) throws IllegalArgumentException {
        Sequence sequence = new Sequence(sequenceType);
        sequence.setFastaHeader(header);

        // Remove ">" and split on pipe.
        String[] headerArgs = header.substring(1).split("\\|");

        // Use the first identifier in the header as id.
        if (headerArgs[0].length() > MIN_ID_LENGTH || headerArgs.length == 1) {
            sequence.setId(headerArgs[0]);
        } else {
            sequence.setId(headerArgs[0] + "|" + headerArgs[1]);
        }

        // Description is the last argument of the header. if there is no description,
        // it will be the same as the (last) identifier.
        String description = headerArgs[headerArgs.length - 1];
        organismMatcher.reset(description);

        if (organismMatcher.find()) {
            sequence.setDescription(organismMatcher.group(1));
            sequence.setOrganism(organismMatcher.group(2));
        } else {
            sequence.setDescription(description);
            sequence.setOrganism("unknown");
        }

        return sequence;
    }

    /**
     * Getter for path.
     *
     * @return the path
     */
    public final MultipartFile getFile() {
        return file;
    }

    /**
     * Getter for sequences.
     *
     * @return sequence object
     */
    public final List<Sequence> getSequences() {
        return sequences;
    }

    /**
     * Returns the summary info of the FASTA file.
     *
     * @return the summary as a formatted string
     */
    public final String getSummary() {
        String summary = String.format("%-25s%s\n", "file", file.getOriginalFilename())
                + String.format("%-25s%s\n", "sequence types", sequenceType)
                + String.format("%-25s%s\n", "number of sequences", sequenceCount)
                + String.format("%-25s%s\n", "average sequence length", getAverageLength());

        return summary;
    }

    /**
     * Gets for the number of processed sequences.
     *
     * @return number of sequences processed
     */
    public final int getCount() {
        return sequenceCount;
    }

    /**
     * Validates a FASTA header.
     *
     * @param fastaHeader FASTA header to be validated
     * @return true or false if valid FASTA header
     */
    private boolean validFastaHeader(final String fastaHeader) {
        return fastaHeader.startsWith(">");
    }

    /**
     * Validates and adds the StringBuilder string to the Sequence object.
     *
     * @param sequence the Sequence object
     * @param sb StringBuilder holding the sequence
     */
    private void addSequence(final Sequence sequence, final StringBuilder sb) {
        String seq = sb.toString();

        // Set sequence checks the sequence type
        try {
            sequence.setSequence(seq);
            this.sequenceType = sequence.getSequenceType();
            this.totalLength += seq.length();
        } catch (IllegalArgumentException e) {
            this.invalidCount++;
        }

        this.sequenceCount++;
    }

    /**
     * Notifies listener when reading file is started.
     */
    private void notifyStart() {
        if (fastaListener != null) {
            fastaListener.start();
        }
    }

    /**
     * Notifies listener when sequence object is created. or adds it to a list, if listener is absent
     *
     * @param sequence The sequence object
     */
    private void notifyOrStore(final Sequence sequence) {
        if (fastaListener != null) {
            fastaListener.sequenceCreated(sequence);
        } else {
            sequences.add(sequence);
        }
    }

    /**
     * Notifies listener when file is read.
     */
    private void notifyFinished() {
        if (fastaListener != null) {
            fastaListener.finished(invalidCount);
        }
    }

    /**
     * Return the average length of all the processed sequences.
     *
     * @return average length as a double
     */
    private double getAverageLength() {
        if (sequenceCount == 0) {
            return 0;
        } else {
            return totalLength / sequenceCount;
        }
    }

    @Override
    public String toString() {
        return "FastaHandler{" + "path=" + file + ", sequenceType=" + sequenceType + ", sequenceCount="
                + sequenceCount + ", totalLength=" + totalLength + ", invalidCount=" + invalidCount + '}';
    }

}
