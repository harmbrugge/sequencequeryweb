/*
 * Copyright (c) 2015 Harm Brugge [harmbrugge@gmail.com].
 * All rights reserved.
 */
package com.harmbrugge.sequence_query;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Supported sequence types for a sequence object.
 * The sequence type can be checked through {@link #isSequenceType}
 *
 * @see Sequence
 *
 * @author Harm Brugge
 */
public enum SequenceType {

    /**
     * Protein sequence. containing amino acids
     */
    PROTEIN {
                /**
                 * Matcher to be reused for performance.
                 */
                private final Matcher matcher = Pattern.compile("^[ILKMFTWVRHANDCEQGPSYBZJXU*]+$").matcher("");

                @Override
                public boolean isSequenceType(final String seq) {
                    return matcher.reset(seq).matches();
                }
            },
    /**
     * DNA sequence. containing [A/T/C/G/N]
     */
    DNA {
                /**
                 * Matcher to be reused for performance.
                 */
                private final Matcher matcher = Pattern.compile("^[ATCGN]+$").matcher("");

                @Override
                public boolean isSequenceType(final String seq) {
                    return matcher.reset(seq).matches();
                }
            },
    /**
     * RNA sequence. containing [A/U/C/G/N]
     */
    RNA {
                /**
                 * Matcher to be reused for performance.
                 */
                private final Matcher matcher = Pattern.compile("^[AUCGN]+$").matcher("");

                @Override
                public boolean isSequenceType(final String seq) {
                    return matcher.reset(seq).matches();
                }
            },
    /**
     * Undefined.
     */
    UNDEFINED {
                @Override
                public boolean isSequenceType(final String seq) {
                    return true;
                }
            };

    /**
     * Return true if sequence complies to the sequence type pattern.
     *
     * @param seq Biological sequence as a string
     * @return true if complies to sequence type pattern.
     */
    public abstract boolean isSequenceType(final String seq);

}
