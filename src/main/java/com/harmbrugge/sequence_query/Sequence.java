/*
 * Copyright (c) 2015 Harm Brugge [harmbrugge@gmail.com].
 * All rights reserved.
 */
package com.harmbrugge.sequence_query;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Class responsible for holding and processing biological sequence data.
 * Can be initialized with a sequence type {@link SequenceType}, otherwise the sequence type is determined when the
 * actual sequence is set.
 *
 * @author Harm Brugge
 */
public class Sequence {

    /**
     * Map to hold the molecular weights of nucleotides.
     */
    private static final HashMap<Character, Double> MOL_WEIGHTS_NUC;

    /**
     * Map to hold the molecular weights of amino acids.
     */
    private static final HashMap<Character, Double> MOL_WEIGHTS_AMINO;

    /**
     * Static initializer for molecular weights.
     */
    static {
        MOL_WEIGHTS_NUC = new HashMap<>();
        MOL_WEIGHTS_NUC.put('A', 331.2);
        MOL_WEIGHTS_NUC.put('C', 307.2);
        MOL_WEIGHTS_NUC.put('G', 347.2);
        MOL_WEIGHTS_NUC.put('T', 322.2);
        MOL_WEIGHTS_NUC.put('U', 324.2);

        MOL_WEIGHTS_AMINO = new HashMap<>();
        MOL_WEIGHTS_AMINO.put('I', 131.1736);
        MOL_WEIGHTS_AMINO.put('L', 131.1736);
        MOL_WEIGHTS_AMINO.put('K', 146.1882);
        MOL_WEIGHTS_AMINO.put('M', 149.2124);
        MOL_WEIGHTS_AMINO.put('F', 165.1900);
        MOL_WEIGHTS_AMINO.put('T', 119.1197);
        MOL_WEIGHTS_AMINO.put('W', 204.2262);
        MOL_WEIGHTS_AMINO.put('V', 117.1469);
        MOL_WEIGHTS_AMINO.put('R', 174.2017);
        MOL_WEIGHTS_AMINO.put('H', 155.1552);
        MOL_WEIGHTS_AMINO.put('A', 89.0935);
        MOL_WEIGHTS_AMINO.put('N', 132.1184);
        MOL_WEIGHTS_AMINO.put('D', 133.1032);
        MOL_WEIGHTS_AMINO.put('C', 121.1590);
        MOL_WEIGHTS_AMINO.put('E', 147.1299);
        MOL_WEIGHTS_AMINO.put('Q', 146.1451);
        MOL_WEIGHTS_AMINO.put('G', 75.0669);
        MOL_WEIGHTS_AMINO.put('P', 115.1310);
        MOL_WEIGHTS_AMINO.put('S', 105.0930);
        MOL_WEIGHTS_AMINO.put('Y', 181.1894);
    }

    /**
     * Sequence type of the sequence.
     */
    private SequenceType sequenceType;

    /**
     * The id of the sequence. GI, GenBank or different identifier. must be unique
     */
    private String id;

    /**
     * Holds the search results if a sequence is searched.
     */
    private List<SearchResult> searchResults;

    /**
     * Identifier in the datasource.
     */
    private int dbId;

    /**
     * The id of the sequence. GI, GenBank or different identifier
     */
    private String externalId;

    /**
     * Textual description of the sequence.
     */
    private String description;

    /**
     * Organism in which the sequence is found.
     */
    private String organism;

    /**
     * String representation of sequence, either in amino acids or nucleotides.
     */
    private String sequence;

    /**
     * Complete FASTA header.
     */
    private String fastaHeader;

    /**
     * Constructor.
     */
    public Sequence() {
        this(SequenceType.UNDEFINED);
    }

    /**
     * Constructor with sequenceType.
     *
     * @param seqType the sequence type of the sequence
     */
    public Sequence(final SequenceType seqType) {
        if (seqType != null) {
            this.sequenceType = seqType;
        } else {
            this.sequenceType = SequenceType.UNDEFINED;
        }
    }

    public int getDbId() {
        return dbId;
    }

    public void setDbId(int dbId) {
        this.dbId = dbId;
    }

    /**
     * Getter for id.
     *
     * @return Id String
     */
    public final String getId() {
        return id;
    }

    /**
     * Setter for Id.
     *
     * @param id Id String
     */
    public final void setId(final String id) {
        this.id = id;
    }

    public List<SearchResult> getSearchResults() {
        return searchResults;
    }

    public void setSearchResults(List<SearchResult> searchResults) {
        this.searchResults = searchResults;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }



    /**
     * Gets the sequence type.
     *
     * @return the sequence type
     */
    public final SequenceType getSequenceType() {
        return sequenceType;
    }

    /**
     * Sets the sequence type.
     *
     * @param sequenceType the sequence type
     */
    public final void setSequenceType(final SequenceType sequenceType) {
        this.sequenceType = sequenceType;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public final String getDescription() {
        return description;
    }

    /**
     * Sets the description.
     *
     * @param description the description
     */
    public final void setDescription(final String description) {
        this.description = description;
    }

    /**
     * Gets the organism name.
     *
     * @return the organism name
     */
    public final String getOrganism() {
        return organism;
    }

    /**
     * Set the organism name.
     *
     * @param organism organism name
     */
    public final void setOrganism(final String organism) {
        this.organism = organism;
    }



    /**
     * Gets the sequence.
     *
     * @return the sequence
     */
    public final String getSequence() {
        return sequence;
    }

    /**
     * Sets the sequence. checks the sequence before setting.
     *
     * @param sequence the sequence
     * @throws IllegalArgumentException if the sequence doesn't comply to one of the sequence types
     */
    public final void setSequence(final String sequence) throws IllegalArgumentException {
        if (sequenceType == SequenceType.UNDEFINED) {
            sequenceType = determineSequenceType(sequence);
        } else if (!sequenceType.isSequenceType(sequence)) {
            throw new IllegalArgumentException("Sequence doesn't comply to sequence type: " + sequenceType);
        }
        this.sequence = sequence;

    }

    /**
     * Gets the FASTA header.
     *
     * @return the FASTA header
     */
    public final String getFastaHeader() {
        return fastaHeader;
    }

    /**
     * Sets the FASTA header.
     *
     * @param fastaHeader the FASTA header
     */
    public final void setFastaHeader(final String fastaHeader) {
        this.fastaHeader = fastaHeader;
    }

    /**
     * Gets the molecular weight of the complete sequence.
     *
     * @return weight in g/mol
     */
    public final double getMolecularWeight() {
        if (sequence == null) {
            return 0.0;
        }
        double weightTotal = 0.0;

        // Get the nr of occurrences of every character in the sequence
        Map<Character, Integer> occur = getOccurrences();

        // Loop over the characters and get the mol weigth for different sequence types
        switch (sequenceType) {
            case PROTEIN:
                for (Map.Entry<Character, Integer> entry : occur.entrySet()) {
                    if (MOL_WEIGHTS_AMINO.containsKey(entry.getKey())){
                        double weight = MOL_WEIGHTS_AMINO.get(entry.getKey());
                        weightTotal += weight * entry.getValue();
                    }
                }
                break;
            case DNA:
            case RNA:
                for (Map.Entry<Character, Integer> entry : occur.entrySet()) {
                    if (MOL_WEIGHTS_NUC.containsKey(entry.getKey())) {
                        double weight = MOL_WEIGHTS_NUC.get(entry.getKey());
                        weightTotal += weight * entry.getValue();
                    }
                }
                break;
            default:
               weightTotal = 0.0;
        }

        return round(weightTotal, 1);
    }

    @Override
    public final String toString() {
        return "Sequence{" + "sequenceType=" + sequenceType + ", id=" + id
                + ", description=" + description
                + ", organism=" + organism + '}';
    }

    @Override
    public final int hashCode() {
        int hash = 7;
        hash = 71 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public final boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Sequence other = (Sequence) obj;
        return Objects.equals(this.id, other.id);
    }

    /**
     * Gets the sequence in FASTA format.
     *
     * @return String in FASTA format
     */
    public final String toFasta() {
        return fastaHeader + "\n" + sequence.replaceAll("(.{70})", "$1\n");
    }

    /**
     * Get the sequence in CSV format.
     *
     * @param sep separator used for CSV output
     * @return Sequence in CSV format
     */
    public final String toCsv(final String sep) {
        return this.getId() + sep
                + this.getDescription() + sep
                + this.getOrganism() + sep
                + this.getSequenceType() + sep
                + this.getLength() + sep
                + this.getMolecularWeight();
    }

    /**
     * Returns the sequence type of a string.
     *
     * @param sequence The biological sequence as a string
     * @return the sequence type of the string
     * @throws IllegalArgumentException if the sequence type is not valid
     */
    private SequenceType determineSequenceType(final String sequence) throws IllegalArgumentException {
        if (SequenceType.PROTEIN.isSequenceType(sequence)) {
            return SequenceType.PROTEIN;
        } else if (SequenceType.DNA.isSequenceType(sequence)) {
            return SequenceType.DNA;
        } else if (SequenceType.RNA.isSequenceType(sequence)) {
            return SequenceType.RNA;
        } else {
            throw new IllegalArgumentException("Invalid sequence type");
        }
    }

    /**
     * Returns a map with the number of occurrences of each character in the
     * sequences.
     *
     * @return map with the numbers of occurrences for each character
     */
    private Map<Character, Integer> getOccurrences() {
        Map<Character, Integer> occurrences = new HashMap<>();
        if (sequence == null) {
            return occurrences;
        }
        for (int i = 0; i < sequence.length(); i++) {
            char charAt = sequence.charAt(i);

            if (!occurrences.containsKey(charAt)) {
                occurrences.put(charAt, 1);
            } else {
                occurrences.put(charAt, occurrences.get(charAt) + 1);
            }
        }
        return occurrences;
    }

    /**
     * Return the length of the sequence.
     *
     * @return the length of the sequence
     */
    private int getLength() {
        if (sequence != null) {
            return this.getSequence().length();
        }
        return 0;
    }

    /**
     * Round to certain number of decimals.
     *
     * @param d double to be rounded
     * @param decimalPlace number of decimal places
     * @return rounded double value
     */
    public static final double round(final double d, final int decimalPlace) {
        BigDecimal bd = new BigDecimal(d);
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd.doubleValue();
    }

}
