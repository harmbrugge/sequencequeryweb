/*
 * Copyright (c) 2015 Harm Brugge [harmbrugge@gmail.com].
 * All rights reserved.
 */
package com.harmbrugge.sequence_query;

/**
 * Data holder object for a search result. Produced by
 * {@link SequenceSearcher#search(Sequence)} via {@link Filter} implementations
 *
 * @see SequenceSearcher
 * @see Filter
 *
 * @author Harm Brugge
 */
public class SearchResult {

    /**
     * Search searchOption used the generate result.
     */
    private SearchOption searchOption;

    /**
     * Pattern used for search.
     */
    private String pattern;

    /**
     * String that matches the pattern.
     */
    private String result;

    /**
     * Start location of result in sequence.
     */
    private int start;

    /**
     * Stop location of result in sequence.
     */
    private int stop;

    /**
     * Public constructor.
     */
    public SearchResult() {

    }

    /**
     * Constructor with result, option and start position.
     *
     * @param result the result
     * @param searchOption the search option
     * @param start start location
     */
    public SearchResult(final String result, final SearchOption searchOption,
                        final int start, final int stop) {
        this.result = result;
        this.start = start;
        this.stop = stop;
        this.searchOption = searchOption;
    }

    public SearchOption getSearchOption() {
        return searchOption;
    }

    public void setSearchOption(SearchOption searchOption) {
        this.searchOption = searchOption;
    }

    /**
     * Gets the pattern.
     * @return the pattern used for search
     */
    public final String getPattern() {
        return pattern;
    }

    /**
     * Sets a pattern.
     * @param pattern the pattern used for search
     */
    public final void setPattern(final String pattern) {
        this.pattern = pattern;
    }

    /**
     * Gets the result.
     * @return the search result
     */
    public final String getResult() {
        return result;
    }

    /**
     * Sets the result.
     *
     * @param result the search result
     */
    public final void setResult(final String result) {
        this.result = result;
    }

    /**
     * Gets the start position.
     *
     * @return the start position
     */
    public final int getStart() {
        return start;
    }

    /**
     * Sets the start position.
     *
     * @param start the start position
     */
    public final void setStart(final int start) {
        this.start = start;
    }

    /**
     * Gets the stop position.
     *
     * @return the stop position
     */
    public final int getStop() {
        return stop;
    }

    /**
     * Sets the stop position.
     * @param stop the stop position
     */
    public final void setStop(final int stop) {
        this.stop = stop;
    }

    @Override
    public final String toString() {
        return "SearchResult{" + "searchOption=" + searchOption + ", pattern=" + pattern
                + ", result=" + result + ", start=" + start
                + ", stop=" + stop + '}';
    }

}
