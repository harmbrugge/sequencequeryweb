/*
 * Copyright (c) 2015 Harm Brugge [harmbrugge@gmail.com].
 * All rights reserved.
 */
package com.harmbrugge.sequence_query.web;

import com.harmbrugge.sequence_query.SearchOption;
import com.harmbrugge.sequence_query.SearchResult;
import com.harmbrugge.sequence_query.Sequence;
import com.harmbrugge.sequence_query.SequenceType;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.jdbc.core.RowMapper;

/**
 * Implementation of RowMapper for a sequence object.
 *
 * A group concatenation with string parsing is used to reduce the amount of database calls.
 *
 * @author Harm Brugge
 */
public class SequenceRowMapper implements RowMapper<Sequence> {

    @Override
    public Sequence mapRow(final ResultSet rs, final int rowNum) throws SQLException {

        Sequence sequence = new Sequence(SequenceType.valueOf(rs.getString("sequence_type")));

        sequence.setDbId(rs.getInt("id"));
        sequence.setOrganism(rs.getString("organism"));
        sequence.setDescription(rs.getString("description"));
        sequence.setExternalId(rs.getString("external_id"));
        sequence.setId(rs.getString("external_id"));
        sequence.setFastaHeader(rs.getString("fasta_header"));
        sequence.setSequence(rs.getString("sequence"));

        List<SearchResult> searchResults = new ArrayList<>();

        // Parse the group concat
        String resultString = rs.getString("search_results");
        if (resultString != null) {
            String[] results = resultString.split(",");

            for (String result :results) {
                String[] arguments = result.split(";");
                if (arguments.length >= 3) {
                    searchResults.add(new SearchResult(null, SearchOption.valueOf(arguments[2]),
                            Integer.parseInt(arguments[0]), Integer.parseInt(arguments[1])));
                }
            }
        }

        sequence.setSearchResults(searchResults);

        return sequence;
    }
}
