/*
 * Copyright (c) 2015 Harm Brugge [harmbrugge@gmail.com].
 * All rights reserved.
 */
package com.harmbrugge.sequence_query.web.user;

import java.util.Date;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

/**
 * User class.
 *
 * @author Harm Brugge
 * @version 0.0.1
 */
public class User {

    /**
     * Interface for validation groups.
     */
    interface ValidationBasic {
    }
    /**
     * Interface for validation groups.
     */
    interface ValidationPassword {
    }

    /**
     * Unique database identifier.
     */
    private int id;

    /**
     * Username.
     */
    @NotBlank(groups = {ValidationBasic.class})
    @Size(min = 3, max = 15, groups = {ValidationBasic.class})
    @Pattern(regexp = "^\\w{3,}$", groups = {ValidationBasic.class})
    private String username;

    /**
     * The authority of the user.
     */
    private String authority;

    /**
     * The first name of the user.
     */
    @NotBlank(groups = {ValidationBasic.class})
    private String name;

    /**
     * The last name of the user.
     */
    @NotBlank(groups = {ValidationBasic.class})
    private String surname;

    /**
     * The email address of the user.
     */
    @NotBlank(groups = {ValidationBasic.class})
    @Email(groups = {ValidationBasic.class})
    private String email;

    /**
     * Password of the user. will be encoded before storing in data source.
     */
    @NotBlank(groups = {ValidationPassword.class})
    @Pattern(regexp = "^\\S+$", groups = {ValidationPassword.class})
    @Size(min = 8, max = 15, groups = {ValidationPassword.class})
    private String password;

    /**
     * Is the user enables.
     */
    private boolean enabled;

    /**
     * Password token for password reset.
     * Not yet implemented
     */
    private String passwordToken;

    /**
     * Time stamp when user is created.
     * Not yet implemented
     */
    private Date timestamp;

    public User() {

    }

    /**
     * Constructor with basic credentials.
     *
     * @param enabled
     * @param username
     * @param authority
     * @param name
     * @param surname
     * @param email
     * @param password
     */
    public User(boolean enabled, String username, String authority, String name,
                String surname, String email, String password) {
        this.enabled = enabled;
        this.username = username;
        this.authority = authority;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getPasswordToken() {
        return passwordToken;
    }

    public void setPasswordToken(String passwordToken) {
        this.passwordToken = passwordToken;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", authority='" + authority + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", enabled=" + enabled +
                ", passwordToken='" + passwordToken + '\'' +
                ", timestamp=" + timestamp +
                '}';
    }
}
