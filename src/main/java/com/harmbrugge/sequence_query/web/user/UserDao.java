package com.harmbrugge.sequence_query.web.user;

import java.util.List;

/**
 * @author Harm Brugge
 * @version 0.0.1
 */
public interface UserDao {

    /**
     * Gets all the users.
     * @return all the user;
     */
    List<User> getUsers();

    /**
     * Get the user by username.
     * @param username the username
     * @return user object
     */
    User getUser(String username);

    /**
     * Get the id of the user by username.
     * @param username the username
     * @return the id
     */
    int getId(String username);

    /**
     * Checks if username already exists.
     * @param username the username
     * @return true if exists
     */
    boolean exists(String username);

    /**
     * Check if the email already exists.
     * @param email email address
     * @return true if exists
     */
    boolean existsEmail(String email);

    /**
     * Should create the user in the data source.
     * @param user User object
     * @return true if succeeded
     */
    boolean create(User user);

    /**
     * Updates the user credentials.
     * @param user the user object
     * @return true if succeeded
     */
    boolean updateUser(User user);

    /**
     * Changes the password of the user.
     * @param user user object
     * @return true if succeeded
     */
    boolean changePassword(User user);
}
