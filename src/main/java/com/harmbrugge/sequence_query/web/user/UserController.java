/*
 * Copyright (c) 2016 Harm Brugge [harmbrugge@gmail.com].
 * All rights reserved.
 */
package com.harmbrugge.sequence_query.web.user;

import com.harmbrugge.sequence_query.web.ErrorMessage;
import java.security.Principal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Harm Brugge
 * @version 0.0.1
 */
@RestController
public class UserController {

    /**
     * User service for data source access.
     */
    @Autowired
    private UserService userService;

    /**
     * Messages source for messages.properties.
     */
    @Autowired
    private MessageSource messageSource;

    /**
     * API used to check if a logged in user is on the session.
     * Will return a 204 status (No content) if the user is not logged in.
     *
     * @param user Principal user object on the session
     * @return User object when logged in.
     */
    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public Object user(final Principal user) {
        if (user != null) {
            return userService.getUser(user.getName());
        }
        return new ResponseEntity<String>(HttpStatus.NO_CONTENT);
    }

    /**
     * API for generating a new account.
     * Will return a status 409(conflict) if username or email already exist.
     * A 401 (Bad Request) response will be returned if validation fails.
     *
     * @param user the user account to be generated
     * @return HTTP status created if user is created, conflict if user credentials already exist.
     */
    @RequestMapping(value = "/user", method = RequestMethod.POST)
    public Object newUser(@Validated({User.ValidationBasic.class, User.ValidationPassword.class})
                          @RequestBody final User user) {

        if (userService.exists(user.getUsername())) {
            ErrorMessage error = new ErrorMessage("409", messageSource.getMessage("Exists.user.username", null, null));
            return new ResponseEntity<>(error, HttpStatus.CONFLICT);
        }

        if (userService.existsEmail(user.getEmail())) {
            ErrorMessage error = new ErrorMessage("409", messageSource.getMessage("Exists.user.email", null, null));
            return new ResponseEntity<>(error, HttpStatus.CONFLICT);
        }

        userService.createUser(user);

        return new ResponseEntity<String>(HttpStatus.CREATED);
    }

}
