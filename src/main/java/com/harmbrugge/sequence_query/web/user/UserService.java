/*
 * Copyright (c) 2015 Harm Brugge [harmbrugge@gmail.com].
 * All rights reserved.
 */
package com.harmbrugge.sequence_query.web.user;

import java.security.Principal;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * User service.
 *
 * @author Harm Brugge
 * @version 0.0.1
 */
@Service
public class UserService {

    /**
     * User data access object.
     */
    @Autowired
    private UserDao userDao;

    /**
     * Password encoder.
     */
    @Autowired
    private PasswordEncoder passwordEncoder;

    /**
     * Load dummy user.
     */
    @PostConstruct
    private void loadDummyUsers() {
        User dummy = new User();
        dummy.setUsername("dummy");
        dummy.setPassword(passwordEncoder.encode("12345678"));
        dummy.setAuthority("USER");
        dummy.setEmail("dummy@gmail.com");
        dummy.setEnabled(true);

        userDao.create(dummy);

        User admin = new User();
        admin.setAuthority("ADMIN");
        admin.setUsername("admin");
        admin.setPassword(passwordEncoder.encode("12345678"));
        admin.setEnabled(true);

        userDao.create(admin);
    }

    /**
     * Create a user.
     * encodes the password and puts the user in the data source
     *
     * @param user User object
     * @return true if succeeded
     */
    public boolean createUser(final User user) {
        // encode the password
        user.setPassword(passwordEncoder.encode(user.getPassword()));

        // enable user by default
        user.setEnabled(true);
        user.setAuthority("USER");

        return userDao.create(user);
    }

    /**
     * Update the user credentials.
     *
     * @param user the user
     * @return true if succeeded.
     */
    public boolean updateUser(final User user) {
        // id retrieved from db for security
        user.setId(userDao.getId(user.getUsername()));

        return userDao.updateUser(user);
    }

    /**
     * Changes the password.
     * @param user the user object.
     * @return true if succeeded.
     */
    public boolean changePassword(final User user) {
        // id retrieved from db for security
        user.setId(userDao.getId(user.getUsername()));
        // encode the password
        user.setPassword(passwordEncoder.encode(user.getPassword()));

        return userDao.changePassword(user);
    }

    /**
     * Gets the user. if principal object isn't available get anonymous user.
     * @param principal the principal user session object
     * @return the user
     */
    public User getUser(final Principal principal) {
        if (principal != null) {
            return this.getUser(principal.getName());
        } else {
            return this.getUser("anonymous");
        }
    }

    /**
     * Gets the user by username.
     * @param username username.
     * @return user object.
     */
    public User getUser(final String username) {
        return userDao.getUser(username);
    }

    /**
     * Exists username.
     * @param username username
     * @return true if exists
     */
    public boolean exists(final String username) {
        return userDao.exists(username);
    }

    /**
     * Exists email.
     * @param email email
     * @return true if exists
     */
    public boolean existsEmail(final String email) {
        return userDao.existsEmail(email);
    }

}
