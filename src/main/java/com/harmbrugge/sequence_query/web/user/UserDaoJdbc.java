/*
 * Copyright (c) 2015 Harm Brugge [harmbrugge@gmail.com].
 * All rights reserved.
 */
package com.harmbrugge.sequence_query.web.user;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

/**
 * User data access object. implementation for JDBC
 *
 * @author Harm Brugge
 * @version 0.0.1
 */
@Component("userDao")
public class UserDaoJdbc implements UserDao {

    /**
     * JDBC template for querying the database.
     */
    @Autowired
    private NamedParameterJdbcTemplate jdbc;

    @Override
    public List<User> getUsers() {
        return null;
    }

    @Override
    public User getUser(final String username) {
        return jdbc.queryForObject("SELECT * FROM users WHERE username =:username",
                new MapSqlParameterSource("username", username),
                BeanPropertyRowMapper.newInstance(User.class));
    }

    @Override
    public int getId(final String username) {
        return jdbc.queryForObject("SELECT id FROM users WHERE username =:username",
                new MapSqlParameterSource("username", username), Integer.class);
    }

    @Override
    public boolean exists(final String username) {
        return jdbc.queryForObject(
                "SELECT count(*) FROM users WHERE username=:username",
                new MapSqlParameterSource("username", username), Integer.class) > 0;
    }

    @Override
    public boolean existsEmail(final String email) {
        return jdbc.queryForObject(
                "SELECT count(*) FROM users WHERE email=:email",
                new MapSqlParameterSource("email", email), Integer.class) > 0;
    }

    @Override
    public boolean create(final User user) {
        return jdbc.update("INSERT INTO users (username, authority, name, surname, email, password, enabled) "
                         + "VALUES (:username, :authority, :name, :surname, :email, :password, :enabled)",
                new BeanPropertySqlParameterSource(user)) == 1;
    }

    @Override
    public boolean updateUser(final User user) {
        return jdbc.update("UPDATE users SET name =:name, surname =:surname, email =:email WHERE id =:id",
                new BeanPropertySqlParameterSource(user)) == 1;
    }

    @Override
    public boolean changePassword(final User user) {
        return jdbc.update("UPDATE users SET password =:password WHERE id =:id",
                new BeanPropertySqlParameterSource(user)) == 1;
    }

}
