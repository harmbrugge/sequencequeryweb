/*
 * Copyright (c) 2015 Harm Brugge [harmbrugge@gmail.com].
 * All rights reserved.
 */
package com.harmbrugge.sequence_query.web;

import com.harmbrugge.sequence_query.web.user.User;
import java.util.Date;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * Class for holding search request options for the web form.
 *
 * @author harmbrugge
 */
public class SearchRequest {

    /**
     * Database id.
     */
    private int id;

    /**
     * The user owning the search request
     */
    private User user;

    /**
     * Uploaded FASTA file.
     */
    private MultipartFile file;

    /**
     * FASTA file name.
     */
    private String fileName;

    /**
     * Date when search request is started.
     */
    private Date date;

    /**
     * Regular expression string for searching the identifier.
     */
    private String identifier;

    /**
     * Regular expression string for searching the organisms name.
     */
    private String organism;

    /**
     * Regular expression string for searching the description.
     */
    private String description;

    /**
     * Regular expression string for searching the biological sequence.
     */
    private String regex;

    /**
     * PROSITE pattern string for searching the biological sequence.
     */
    private String prosite;

    /**
     * Public constructor.
     */
    public SearchRequest() {

    }

    /**
     * Getter for id.
     * @return id
     */
    public int getId() {
        return id;
    }

    /**
     * Setter for id.
     * @param id id
     */
    public void setId(final int id) {
        this.id = id;
    }

    /**
     * Getter for file.
     * @return file
     */
    public MultipartFile getFile() {
        return file;
    }

    /**
     * Setter for file.
     * @param file file
     */
    public void setFile(final MultipartFile file) {
        this.file = file;
        this.fileName = file.getOriginalFilename();
    }

    /**
     * Setter for file name.
     * @param fileName the file name
     */
    public void setFileName(final String fileName) {
        this.fileName = fileName;
    }

    /**
     * Getter for the file name.
     * @return the file name
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * Getter for the start date.
     * @return the start date
     */
    public Date getDate() {
        return date;
    }

    /**
     * Setter for the start date.
     * @param date the start date
     */
    public void setDate(final Date date) {
        this.date = date;
    }

    /**
     * Getter for identifier.
     * @return the identifier
     */
    public String getIdentifier() {
        return identifier;
    }

    /**
     * Setter for identifier.
     * @param id the identifier
     */
    public void setIdentifier(final String id) {
        this.identifier = id;
    }

    /**
     * Getter for organism.
     * @return the organism
     */
    public String getOrganism() {
        return organism;
    }

    /**
     * Setter for organism.
     * @param organism the organism
     */
    public void setOrganism(final String organism) {
        this.organism = organism;
    }

    /**
     * Getter for the description.
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Setter for the description.
     * @param description the description
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * Getter for regular expression.
     * @return the regular expression
     */
    public String getRegex() {
        return regex;
    }

    /**
     * Setter for regular expression.
     * @param regex the regular expression
     */
    public void setRegex(final String regex) {
        this.regex = regex;
    }

    /**
     * Getter for PROSITE pattern.
     * @return the PROSITE pattern
     */
    public String getProsite() {
        return prosite;
    }

    /**
     * Setter for PROSITE pattern.
     * @param prosite the PROSITE pattern
     */
    public void setProsite(final String prosite) {
        this.prosite = prosite;
    }

    /**
     * Getter for the user.
     * @return the user object
     */
    public User getUser() {
        return user;
    }

    /**
     * Setter for the user.
     * @param user the user object
     */
    public void setUser(final User user) {
        this.user = user;
    }

    /**
     * Getter for user id.
     * @return the user id if user in not null
     */
    public Integer getUserId() {
        if (user != null) {
            return user.getId();
        }
        return null;
    }

    @Override
    public String toString() {
        return "SearchRequest{" + "id=" + id
                + ", file=" + file
                + ", fileName=" + fileName
                + ", date=" + date
                + ", identifier=" + identifier
                + ", organism=" + organism
                + ", description=" + description
                + ", regex=" + regex
                + ", prosite=" + prosite + '}';
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SearchRequest that = (SearchRequest) o;

        return id == that.id;

    }

    @Override
    public int hashCode() {
        return id;
    }
}
