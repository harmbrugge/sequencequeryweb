/*
 * Copyright (c) 2015 Harm Brugge [harmbrugge@gmail.com].
 * All rights reserved.
 */
package com.harmbrugge.sequence_query.web;

import com.harmbrugge.sequence_query.SearchResult;
import com.harmbrugge.sequence_query.Sequence;
import com.harmbrugge.sequence_query.web.user.User;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Component;

/**
 * Implementation of the SequenceDao interface for JDBC.
 *
 * @author Harm Brugge
 */
@Component("sequenceDao")
public class SequenceDaoJdbc implements SequenceDao {

    /**
     * JDBC template for querying the database.
     */
    @Autowired
    private NamedParameterJdbcTemplate namedJdbc;

    /**
     * JDBC template for simple queries.
     */
    @Autowired
    private JdbcTemplate jdbc;

    @Override
    public List<Sequence> getSequences() {
        return jdbc
                .query("SELECT * FROM sequences", BeanPropertyRowMapper.newInstance(Sequence.class));
    }

    @Override
    public void addSequence(final Sequence sequence, final int searchRequestId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("id", sequence.getId());
        params.addValue("description", sequence.getDescription());
        params.addValue("organism", sequence.getOrganism());
        params.addValue("sequence", sequence.getSequence());
        params.addValue("fastaHeader", sequence.getFastaHeader());
        params.addValue("sequenceType", sequence.getSequenceType().name());
        params.addValue("searchRequestId", searchRequestId);

        GeneratedKeyHolder key = new GeneratedKeyHolder();

        namedJdbc.update("INSERT INTO sequences"
                + "(external_id, description, organism, sequence, fasta_header, sequence_type, search_request_id) "
                + "VALUES (:id, :description, :organism, :sequence, :fastaHeader, :sequenceType, :searchRequestId)",
                params, key);

        sequence.setDbId(key.getKey().intValue());
    }

    @Override
    public List<Sequence> getSequences(final int searchRequestId) {
        return namedJdbc
                .query("SELECT s.*, GROUP_CONCAT(CONCAT(r.start,';', r.stop, ';', r.search_option)) search_results "
                        + "FROM sequences s LEFT JOIN search_results r ON r.sequence_id = s.id "
                        + "WHERE search_request_id = :searchRequestId GROUP BY s.id ORDER BY s.id",
                        new MapSqlParameterSource("searchRequestId", searchRequestId),
                        new SequenceRowMapper());
    }

    @Override
    public void addSearchResult(final SearchResult result, final Sequence sequence) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("start", result.getStart());
        params.addValue("searchOption", result.getSearchOption().name());
        params.addValue("stop", result.getStop());
        params.addValue("sequenceId", sequence.getDbId());

        namedJdbc.update("INSERT INTO search_results (sequence_id, start, stop, search_option) "
                + "VALUES (:sequenceId, :start, :stop, :searchOption)", params);
    }

    @Override
    public int addSearchRequest(final SearchRequest searchRequest) {
        GeneratedKeyHolder key = new GeneratedKeyHolder();

        namedJdbc.update("INSERT INTO search_requests "
                + "(user_id, date, file_name, identifier, organism, regex, prosite, description) "
                + "VALUES (:userId, NOW(), :fileName, :identifier, :organism, :regex, :prosite, :description)",
                new BeanPropertySqlParameterSource(searchRequest), key);

        return key.getKey().intValue();
    }

    @Override
    public List<SearchRequest> getSearchRequests() {
        return jdbc.query("SELECT sr.id, sr.identifier, sr.date, sr.file_name, "
                + "               sr.organism, sr.regex, sr.prosite, sr.description "
                + "           FROM search_requests sr ORDER BY id DESC LIMIT 5",
                BeanPropertyRowMapper.newInstance(SearchRequest.class));
    }

    @Override
    public List<SearchRequest> getSearchRequests(final User user, final int start, final int end) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("id", user.getId());
        params.addValue("start", start);
        params.addValue("end", end);

        return namedJdbc.query("SELECT sr.id, sr.identifier, sr.date, sr.file_name, "
                        + "               sr.organism, sr.regex, sr.prosite, sr.description "
                        + "           FROM search_requests sr WHERE user_id =:id ORDER BY id DESC LIMIT :start, :end",
                params,
                BeanPropertyRowMapper.newInstance(SearchRequest.class));
    }

    @Override
    public SearchRequest getSearchRequest(final int id) {
        return namedJdbc.queryForObject("SELECT sr.id, sr.identifier, sr.date, sr.file_name, "
                        + "                     sr.organism, sr.regex, sr.prosite, sr.description "
                        + "                 FROM search_requests sr WHERE sr.id =:id"
                , new MapSqlParameterSource("id", id), BeanPropertyRowMapper.newInstance(SearchRequest.class));
    }
}
