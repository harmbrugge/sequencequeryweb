/*
 * Copyright (c) 2015 Harm Brugge [harmbrugge@gmail.com].
 * All rights reserved.
 */
package com.harmbrugge.sequence_query.web;

import com.harmbrugge.sequence_query.FastaListener;
import com.harmbrugge.sequence_query.Filter;
import com.harmbrugge.sequence_query.OptionService;
import com.harmbrugge.sequence_query.SearchOption;
import com.harmbrugge.sequence_query.SearchResult;
import com.harmbrugge.sequence_query.Sequence;
import com.harmbrugge.sequence_query.SequenceSearcher;
import com.harmbrugge.sequence_query.filter.DescriptionFilter;
import com.harmbrugge.sequence_query.filter.IdFilter;
import com.harmbrugge.sequence_query.filter.OrganismFilter;
import com.harmbrugge.sequence_query.filter.SequenceFilter;
import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of the OptionService interface for web.
 *
 * Writes the generated sequences to a data source
 *
 * @author Harm Brugge
 */
public class OptionsServiceWeb implements OptionService {

    /**
     * Search request. holding the option form the web form
     */
    private final SearchRequest searchRequest;

    /**
     * Database access object.
     */
    private final SequenceDao sequenceDao;

    /**
     * Inserted id in database.
     */
    private int searchId;

    /**
     * Public constructor.
     *
     * @param searchRequest the search request holding the
     * @param sequenceDao Sequence sequenceDao object
     */
    public OptionsServiceWeb(final SearchRequest searchRequest, final SequenceDao sequenceDao) {
        this.searchRequest = searchRequest;
        this.sequenceDao = sequenceDao;
    }

    @Override
    public String getInputPath() {
        return searchRequest.getFile().getOriginalFilename();
    }

    @Override
    public FastaListener getListener() {
        searchId = sequenceDao.addSearchRequest(searchRequest);

        final SequenceSearcher searcher = getSequenceSearcher();

        return new FastaListener() {
            @Override
            public void start() {
                // Do nothing
            }

            @Override
            public void sequenceCreated(final Sequence sequence) {
                if (searcher.search(sequence)) {
                    sequenceDao.addSequence(sequence, searchId);

                    if (searcher.hasSeqResult()) {
                        for (SearchResult result :searcher.getResults()) {
                            sequenceDao.addSearchResult(result, sequence);
                        }
                    }
                }
            }

            @Override
            public void finished(final int invalidCount) {
                // do nothing
            }
        };
    }

    @Override
    public boolean hasSummary() {
        return false;
    }

    /**
     * Returns the inserted database id.
     *
     * @return the inserted database id
     */
    public int getSearchId() {
        return searchId;
    }

    /**
     * Returns a list of filters for the selected search options.
     *
     * @return list of sequence filters
     */
    private List<Filter> getFilters() {
        List<Filter> filters = new ArrayList<>();

        if (searchRequest.getDescription() != null) {
            filters.add(new DescriptionFilter(searchRequest.getDescription()));
        }
        if (searchRequest.getOrganism() != null) {
            filters.add(new OrganismFilter(searchRequest.getOrganism()));
        }
        if (searchRequest.getIdentifier() != null) {
            filters.add(new IdFilter(searchRequest.getIdentifier()));
        }
        if (searchRequest.getProsite() != null) {
            filters.add(new SequenceFilter(searchRequest.getProsite(), SearchOption.PROSITE));
        }
        if (searchRequest.getRegex() != null) {
            filters.add(new SequenceFilter(searchRequest.getRegex()));
        }

        return filters;
    }

    /**
     * Returns a sequence search object with chosen filters.
     *
     * @return the sequence search object with filters
     */
    private SequenceSearcher getSequenceSearcher() {
        return new SequenceSearcher(getFilters());
    }

}
