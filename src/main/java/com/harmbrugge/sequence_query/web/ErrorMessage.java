/*
 * Copyright (c) 2016 Harm Brugge [harmbrugge@gmail.com].
 * All rights reserved.
 */
package com.harmbrugge.sequence_query.web;

/**
 * @author Harm Brugge
 * @version 0.0.1
 */
public class ErrorMessage {

    /**
     * HTTP Status.
     */
    private String status;

    /**
     * The error message.
     */
    private String message;

    /**
     * Default constructor.
     */
    public ErrorMessage() {

    }

    /**
     * Constructor with all options.
     *
     * @param status HTTP status
     * @param message the error message
     */
    public ErrorMessage(final String status, final String message) {
        this.status = status;
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
