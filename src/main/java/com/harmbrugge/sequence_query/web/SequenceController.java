/*
 * Copyright (c) 2015 Harm Brugge [harmbrugge@gmail.com].
 * All rights reserved.
 */
package com.harmbrugge.sequence_query.web;

import com.harmbrugge.sequence_query.FastaHandler;
import com.harmbrugge.sequence_query.Sequence;
import com.harmbrugge.sequence_query.web.user.User;
import com.harmbrugge.sequence_query.web.user.UserService;
import java.io.IOException;
import java.security.Principal;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * Rest API controller for sequence query.
 * Handles all the data requests
 *
 * @author Harm Brugge
 */
@RestController
public class SequenceController {

    /**
     * Sequence DAO. handles the data requests from and to the database
     */
    @Autowired
    private SequenceDao sequenceDao;

    /**
     * User service.
     */
    @Autowired
    private UserService userService;

    /**
     * API for getting the sequences belonging to a search request.
     *
     * @param id the database identifier of a search request
     * @return a list of sequences
     */
    @RequestMapping(value = "/sequences/{id}", method = RequestMethod.GET)
    public List<Sequence> getSequences(@PathVariable final int id) {
        return sequenceDao.getSequences(id);
    }

    /**
     * API for getting all the search request for the logged in user.
     * limited by start en end parameters
     *
     * @param principal the user object on session
     * @param start start index
     * @param end end index
     * @return the search request objects
     */
    @RequestMapping(value = "/searches", method = RequestMethod.GET)
    public List<SearchRequest> getSearches(final Principal principal,
                                           @RequestParam(value = "start", defaultValue = "0") final int start,
                                           @RequestParam(value = "end", defaultValue = "6") final int end) {
        User user = userService.getUser(principal);

        return sequenceDao.getSearchRequests(user, start, end);
    }

    /**
     * API for getting a single search request.
     *
     * @param id the search requests id
     * @return the corresponding search request object
     */
    @RequestMapping(value = "/search-request/{id}", method = RequestMethod.GET)
    public SearchRequest getSearchRequest(@PathVariable final int id) {
        return sequenceDao.getSearchRequest(id);
    }

    /**
     * API for posting a search request.
     * Form data is mapped on a search request object.
     *
     * @param principal user object on session
     * @param searchRequest the search request
     * @return the identifier at data source
     * @throws IOException when reading the FASTA file fails
     */
    @RequestMapping(value = "/search-request", method = RequestMethod.POST)
    public int doSearchRequest(final SearchRequest searchRequest,
                               final Principal principal) throws IOException {
        searchRequest.setUser(userService.getUser(principal));

        OptionsServiceWeb optionService = new OptionsServiceWeb(searchRequest, sequenceDao);

        FastaHandler fastaHandler = new FastaHandler(searchRequest.getFile(), optionService.getListener());
        fastaHandler.readFile();

        return optionService.getSearchId();
    }

    /**
     * Error handler for Illegal arguments exceptions.
     * Responds with a 401(Bad request) if exception is raised and returns the exception as JSON
     *
     * @param exception the raised illegal argument exception
     * @return the exception
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(IllegalArgumentException.class)
    public Object argumentException(final IllegalArgumentException exception) {
        return exception;
    }

}
