package com.harmbrugge.sequence_query.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Entry point for the web application.
 *
 * This class should be changed if WAR packaging is required
 *
 * @author Harm Brugge
 */
@SpringBootApplication
public class SequenceQueryApplication {
//@SpringBootApplication
//public class SequenceQueryApplication extends SpringBootServletInitializer {

//    @Override
//    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
//        return application.sources(SequenceQueryApplication.class);
//    }
    /**
     * Main.
     * @param args arguments
     */
    public static void main(final String[] args) {
        SpringApplication.run(SequenceQueryApplication.class, args);
    }

}
