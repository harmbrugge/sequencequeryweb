/*
 * Copyright (c) 2015 Harm Brugge [harmbrugge@gmail.com].
 * All rights reserved.
 */
package com.harmbrugge.sequence_query.web;

import com.harmbrugge.sequence_query.SearchResult;
import com.harmbrugge.sequence_query.Sequence;
import com.harmbrugge.sequence_query.web.user.User;
import java.util.List;

/**
 * Database access object for sequences.
 *
 * @author Harm Brugge
 */
public interface SequenceDao {

    /**
     * Should return all the sequences in the data source.
     *
     * @return List of sequence objects
     */
    List<Sequence> getSequences();

    /**
     * Adds a sequence to a search request in the data source.
     * Should add the database id to the sequence object
     *
     * @param sequence the sequence to add
     * @param requestId the id of the search request
     */
    void addSequence(Sequence sequence, int requestId);

    /**
     * Returns all the sequences belonging to a search request.
     *
     * @param requestId the id of a search request
     * @return the sequences belonging to the search request
     */
    List<Sequence> getSequences(int requestId);

    /**
     *
     * Adds a search result to the data source.
     *
     * @param result the search result of a sequence
     * @param sequence the corresponding sequence
     */
    void addSearchResult(SearchResult result, Sequence sequence);

    /**
     *
     * Adds a search request to the data source.
     *
     * @param searchRequest the search request
     * @return the identifier created at the data source
     */
    int addSearchRequest(SearchRequest searchRequest);

    /**
     * Returns all the search requests in the data source.
     *
     * @return the search requests
     */
    List<SearchRequest> getSearchRequests();

    /**
     * Returns the search requests in the data source for a user.
     *
     * @param user the logged in user
     * @param start start index
     * @param end end index
     * @return the search requests
     */
    List<SearchRequest> getSearchRequests(User user, int start, int end);


    /**
     * Gets the search request of the corresponding identifier.
     *
     * @param id the search requests id
     * @return the search request
     */
    SearchRequest getSearchRequest(int id);

}
