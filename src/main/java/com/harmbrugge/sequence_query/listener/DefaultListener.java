/*
 * Copyright (c) 2015 Harm Brugge [harmbrugge@gmail.com].
 * All rights reserved.
 */
package com.harmbrugge.sequence_query.listener;

import com.harmbrugge.sequence_query.FastaListener;
import com.harmbrugge.sequence_query.Sequence;
import com.harmbrugge.sequence_query.SequenceSearcher;

/**
 * Implementation of the FastaListener interface for generating FASTA output.
 *
 * @author Harm Brugge
 */
public class DefaultListener implements FastaListener {

    /**
     * SequenceSearcher object used to filter the sequences.
     */
    private final SequenceSearcher sequenceSearcher;

    /**
     * Construct with SequenceSearcher object.
     *
     * @param seqSearcher the SequenceSearcher object
     */
    public DefaultListener(final SequenceSearcher seqSearcher) {
        this.sequenceSearcher = seqSearcher;
    }

    @Override
    public final void sequenceCreated(final Sequence sequence) {
        if (sequenceSearcher.search(sequence)) {
            System.out.println(sequence.toFasta());
        }
    }

    @Override
    public final void finished(final int invalidCount) {
        if (sequenceSearcher.hasFilter()) {
            // When finished show amounts of fitered sequences.
            System.out.println(System.lineSeparator() + "[" + sequenceSearcher.getFoundCount() + "/"
                    + sequenceSearcher.getCount()
                    + "] sequences comply to selected filters");

            // Inform user of invalid sequences
            if (invalidCount > 0) {
                System.out.println("[Warning] " + invalidCount
                        + " Sequences omitted because of illegal sequence format");
            }
        }
    }

    @Override
    public final void start() {
        // No action
    }

}
