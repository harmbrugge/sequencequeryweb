/*
 * Copyright (c) 2015 Harm Brugge [harmbrugge@gmail.com].
 * All rights reserved.
 */
package com.harmbrugge.sequence_query.listener;

import com.harmbrugge.sequence_query.FastaListener;
import com.harmbrugge.sequence_query.Sequence;

/**
 * Implementation of the FastaListener interface for generating CSV output.
 *
 * @author Harm Brugge
 */
public class CsvListener implements FastaListener {

    /**
     * The separator for CSV output.
     */
    private final String sep;

    /**
     * Constructor with separator.
     *
     * @param separator separator for the CSV output
     */
    public CsvListener(final String separator) {
        this.sep = separator;
    }

    @Override
    public final void sequenceCreated(final Sequence sequence) {
        System.out.println(sequence.toCsv(sep));
    }

    @Override
    public final void finished(final int invalidCount) {
        // No action
    }

    @Override
    public final void start() {
        // Print top line
        System.out.println("ACCNO" + sep + "NAME" + sep + "ORGANISM" + sep
                           + "TYPE" + sep + "LENGHT" + sep + "MOL_WEIGHT");
    }

}
