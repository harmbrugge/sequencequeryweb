/*
 * Copyright (c) 2015 Harm Brugge [harmbrugge@gmail.com].
 * All rights reserved.
 */
package com.harmbrugge.sequence_query.listener;

import com.harmbrugge.sequence_query.FastaListener;
import com.harmbrugge.sequence_query.Sequence;
import com.harmbrugge.sequence_query.SequenceSearcher;

/**
 * Implementation of the FastaListener interface for sequence searches. Output format is CSV.
 *
 * @author Harm Brugge
 */
public class SeqSearchListener implements FastaListener {

    /**
     * Sequence search object.
     */
    private final SequenceSearcher sequenceSearcher;

    /**
     * Public constructor.
     *
     * @param seqSearcher the sequence search object
     */
    public SeqSearchListener(final SequenceSearcher seqSearcher) {
        this.sequenceSearcher = seqSearcher;
    }

    @Override
    public final void sequenceCreated(final Sequence sequence) {
        if (sequenceSearcher.search(sequence)) {
            System.out.print(sequenceSearcher.resultsToCsv());
        }
    }

    @Override
    public final void start() {
        // Print the top line for CSV output
        System.out.println("ACCNO;NAME;ORGANISM;TYPE;POSITION;SEQ");
    }

    @Override
    public final void finished(final int invalidCount) {
        // When finished show amounts of fitered sequences.
        System.out.println(System.lineSeparator() + "[" + sequenceSearcher.getFoundCount() + "/"
                + sequenceSearcher.getCount()
                + "] sequences comply to selected filters");

        // Inform user of invalid sequences
        if (invalidCount > 0) {
            System.out.println("[Warning] " + invalidCount + " Sequences omitted because of illegal sequence format");
        }
    }

}
