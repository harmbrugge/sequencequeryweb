/*
 * Copyright (c) 2015 Harm Brugge [harmbrugge@gmail.com].
 * All rights reserved.
 */
package com.harmbrugge.sequence_query;

/**
 * Interface for a search filter for a sequence object. can be used in
 * {@link SequenceSearcher}.
 * A list of implementations of this interface can be used when constructing a
 * SequenceSearcher object {@link SequenceSearcher#SequenceSearcher(List)}
 *
 * @see com.harmbrugge.sequence_query.filter.SequenceFilter
 * @see com.harmbrugge.sequence_query.filter.IdFilter
 * @see com.harmbrugge.sequence_query.filter.OrganismFilter
 * @see com.harmbrugge.sequence_query.filter.DescriptionFilter
 *
 * @author Harm Brugge
 */
public interface Filter {

    /**
     * Search method. search criteria can be implemented here. Should return
     * true when criteria are met.
     *
     * @param seq the sequence object
     * @return true when filter criteria are met
     */
    boolean search(Sequence seq);

    /**
     * Getter for search results. Search results can be generated while
     * searching the sequence. May return null if search results are irrelevant
     * {@link SearchResult}
     *
     * @return a search result object
     */
    SearchResult getSearchResult();
}
