/*
 * Copyright (c) 2015 Harm Brugge [harmbrugge@gmail.com].
 * All rights reserved.
 */
package com.harmbrugge.sequence_query;

/**
 * Supported search options for the sequence query application.
 *
 * @see OptionService
 *
 * @author Harm Brugge
 */
public enum SearchOption {

    /**
     * Search by identifiers in FASTA header.
     */
    ID,
    /**
     * Search the sequence with regular expression.
     */
    REG_EX,
    /**
     * Search the sequence with a PROSITE pattern.
     */
    PROSITE,
    /**
     * Search in FASTA file for an organism.
     */
    ORGANISM,
    /**
     * Search in the description of the sequences.
     */
    DESCRIPTION,
    /**
     * Output the complete FASTA file to CSV format.
     */
    TO_CSV
}
