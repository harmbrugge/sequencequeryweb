/*
 * Copyright (c) 2015 Harm Brugge [harmbrugge@gmail.com].
 * All rights reserved.
 */
package com.harmbrugge.sequence_query.filter;

import com.harmbrugge.sequence_query.Filter;
import com.harmbrugge.sequence_query.SearchResult;
import com.harmbrugge.sequence_query.Sequence;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * Implementation of a sequence filter by searching for an identifier.
 *
 * @author Harm Brugge
 */
public class IdFilter implements Filter {

    /**
     * Matcher object reused for every search.
     */
    private final Matcher matcher;

    /**
     * Construct with regular expression.
     *
     * @throws PatternSyntaxException if the regular expression is invalid
     * @param regEx the regular expression
     */
    public IdFilter(final String regEx) throws PatternSyntaxException {
        this.matcher = Pattern.compile(Pattern.quote(regEx)).matcher("");
    }

    /**
     * Search in the FASTA header of a sequence. regular expression provided at
     * object creation.
     *
     * @param seq Sequence to be searched
     * @return true if found
     */
    @Override
    public final boolean search(final Sequence seq) {
        matcher.reset(seq.getFastaHeader());

        return matcher.find();
    }

    /**
     * No action. could be implemented
     *
     * @return null
     */
    @Override
    public final SearchResult getSearchResult() {
        return null;
    }

}
