/*
 * Copyright (c) 2015 Harm Brugge [harmbrugge@gmail.com].
 * All rights reserved.
 */
package com.harmbrugge.sequence_query.filter;

import com.harmbrugge.sequence_query.Filter;
import com.harmbrugge.sequence_query.SearchOption;
import com.harmbrugge.sequence_query.SearchResult;
import com.harmbrugge.sequence_query.Sequence;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * Implementation of a sequence filter by searching the sequence attribute.
 *
 * @author Harm Brugge
 */
public class SequenceFilter implements Filter {

    /**
     * Map holding subtitutions for a PROSITE pattern to a regular expression. Regular expression patterns that need to
     * be replaced are the keys, the replacements are the values.
     */
    private static final Map<String, String> PROSITE_TO_REG_EX;

    static {
        PROSITE_TO_REG_EX = new HashMap<>();
        PROSITE_TO_REG_EX.put("-", "");
        PROSITE_TO_REG_EX.put("x", ".");
        PROSITE_TO_REG_EX.put("\\((.+?)\\)", "{$1}");
        PROSITE_TO_REG_EX.put("(\\[.+?\\])", "$1{1}");
        PROSITE_TO_REG_EX.put("\\{([A-Z]+?)\\}", "[^$1]{1}");
    }

    /**
     * Matcher object reused for every search.
     */
    private final Matcher matcher;

    /**
     * Holds the search result.
     */
    private SearchResult searchResult;

    /**
     * Holds the search option.
     */
    private SearchOption searchOption;

    /**
     * Construct with regular expression.
     *
     * @param regEx the regular expression
     */
    public SequenceFilter(final String regEx) {
        this(regEx, SearchOption.REG_EX);
    }

    /**
     * Construct with regular expression or PROSITE pattern. when a PROSITE pattern is used, supply the search option
     * as well.
     *
     * @param pattern      PROSITE or regular expression pattern
     * @param searchOption the type of the pattern.
     * @throws PatternSyntaxException if the regular expression is invalid
     */
    public SequenceFilter(final String pattern, final SearchOption searchOption) throws PatternSyntaxException {
        if (searchOption == SearchOption.PROSITE) {
            this.matcher = Pattern.compile(prositeToRegEx(pattern)).matcher("");
            this.searchOption = searchOption;
        } else {
            this.matcher = Pattern.compile(pattern).matcher("");
            this.searchOption = SearchOption.REG_EX;
        }
    }

    /**
     * Search the sequence. pattern provided at object creation. search results can be obtained via
     * {@link #getSearchResult()}
     *
     * @param seq the sequence object
     * @return true if pattern is found
     */
    @Override
    public final boolean search(final Sequence seq) {
        if (seq.getSequence() != null) {
            matcher.reset(seq.getSequence());

            if (matcher.find()) {
                searchResult = new SearchResult(matcher.group(0), searchOption, matcher.start(0) + 1,
                                                matcher.end(0) + 1);
                return true;
            }
        }

        return false;
    }

    /**
     * Returns the search result.
     *
     * @return the search result object
     */
    @Override
    public final SearchResult getSearchResult() {
        return searchResult;
    }

    /**
     * Converts a PROSITE pattern into a regular expression.
     *
     * @param prosite the PROSITE pattern
     * @return a regular expression
     */
    private String prositeToRegEx(final String prosite) {
        String regex = prosite;
        for (Map.Entry<String, String> entry : PROSITE_TO_REG_EX.entrySet()) {
            regex = regex.replaceAll(entry.getKey(), entry.getValue());
        }

        return regex;
    }

}
