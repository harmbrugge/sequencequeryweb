/*
 * Copyright (c) 2015 Harm Brugge [harmbrugge@gmail.com].
 * All rights reserved.
 */
package com.harmbrugge.sequence_query;

/**
 * Interface for a FastaHandler listener. A {@link FastaHandler} can be
 * initialized with an implementation of this listener. The listener will be
 * informed:
 *  - Right before the first sequence object is created
 *  - When, while reading the file, a sequence object is created
 *  - When the file is read.
 *
 * @see FastaHandler
 *
 * @author Harm Brugge
 */
public interface FastaListener {

    /**
     * Called when reading starts.
     */
    void start();

    /**
     * Called when sequence object is created. with corresponding sequence
     *
     * @param sequence the sequence object
     */
    void sequenceCreated(Sequence sequence);

    /**
     * Called when reading file is done.
     * @param invalidCount number of invalid sequences in the file
     */
    void finished(int invalidCount);
}
